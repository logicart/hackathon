<?php
	add_action( 'wp_enqueue_scripts', 'tt_child_enqueue_parent_styles' );

	function tt_child_enqueue_parent_styles() {
	   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	}


//custom block for Project List
function acf_project_list() {

	if( function_exists('acf_register_block') ) {

		acf_register_block(array(
			'name'				=> 'project-list',
			'title'				=> __('Проекти'),
			'description'		=> __('Список всіх проектів'),
			'render_template'	=> 'blocks/project-list/project-list.php',
			'category'			=> 'layout',
			'icon'				=> 'editor-ul',
			'keywords'			=> array( 'project' ),
			'enqueue_assets' => function(){
              wp_enqueue_style( 'block-project-list', get_template_directory_uri() . 'blocks/project-list/project-list.css' );
            },
		));
	}
}

add_action('acf/init', 'acf_project_list');


//custom block for staff
function acf_members_list() {

	if( function_exists('acf_register_block') ) {

		acf_register_block(array(
			'name'				=> 'members-list',
			'title'				=> __('Учасники'),
			'description'		=> __('Список всіх учасників'),
			'render_template'	=> 'blocks/members-list/members-list.php',
			'category'			=> 'layout',
			'icon'				=> 'admin-users',
			'keywords'			=> array( 'members' ),
			'enqueue_assets' => function(){
              wp_enqueue_style( 'block-members-list', get_template_directory_uri() . 'blocks/members-list/members-list.css' );
            },
		));
	}
}

add_action('acf/init', 'acf_members_list');

//custom block for CV
function acf_cv() {

	if( function_exists('acf_register_block') ) {

		acf_register_block(array(
			'name'				=> 'cv',
			'title'				=> __('Резюме'),
			'description'		=> __('Резюме учасника'),
			'render_template'	=> 'blocks/cv/cv.php',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'cv' ),
			'enqueue_assets' => function(){
              wp_enqueue_style( 'block-cv', get_template_directory_uri() . 'blocks/cv/cv.css' );
            },
		));
	}
}

add_action('acf/init', 'acf_cv');

//custom block for Project Details
function acf_project_details() {

	if( function_exists('acf_register_block') ) {

		acf_register_block(array(
			'name'				=> 'project-details',
			'title'				=> __('Деталі проекту'),
			'description'		=> __('Детальний опис проекту'),
			'render_template'	=> 'blocks/project-details/project-details.php',
			'category'			=> 'layout',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'project-details' ),
			'enqueue_assets' => function(){
              wp_enqueue_style( 'block-project-details', get_template_directory_uri() . 'blocks/project-details/project-details.css' );
            },
		));
	}
}

add_action('acf/init', 'acf_project_details');

?>